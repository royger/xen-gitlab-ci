#!/bin/sh -ex

export DEBIAN_FRONTEND=noninteractive

# Install docker machine
latest=`curl -s "https://api.github.com/repos/docker/machine/releases/latest" | grep -Po '"tag_name": "\K.*?(?=")'`
curl -L https://github.com/docker/machine/releases/download/$latest/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine
install /tmp/docker-machine /usr/local/bin/docker-machine
rm -f /tmp/docker-machine

# Install gitlab runner
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
export DEBIAN_FRONTEND=noninteractive
apt-get -qy update
apt-get -qy install gitlab-runner
apt-get -qy dist-upgrade
apt-get -qy autoremove --purge

sed -e 's/^PermitRootLogin .*/PermitRootLogin without-password/' \
    -e 's/^PasswordAuthentication .*/PasswordAuthentication no/' \
    -i /etc/ssh/sshd_config
systemctl reload ssh

systemctl enable gitlab-runner
